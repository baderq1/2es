# 3-tiers



## Getting started



- oob mgmt server credentials: ````ubuntu / CumulusLinux!````
- servers credentials: ````ubuntu / nvidia````
- switches credentials: ````cumulus / CumulusLinux!````

## Install latest agent and config switches to netq
- in oob mgmt server : run ./install_config_netq_switches.sh <netq_ip>

## Install latest agent and config servers to netq
- in oob mgmt server : run ./install_config_netq_servers.sh <netq_ip>
